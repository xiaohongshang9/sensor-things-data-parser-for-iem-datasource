var fs = require('fs');
var csv = require('csv');
var path = require('path');
var ids = {};
var tmpcRecords = [];
var dewRecords = [];
//datastream ID in SensorThings service
var id_dew=14604039;
var id_tem=14603136;
//read tmpc or dewpoint CSV file from local disk
var csvFile = process.argv[2];
var csvFolderName = path.dirname(csvFile);

//parse CSV file into sensorThings observations 
var parser = csv.parse({delimiter: '\t', comment: '#'}, function(err, data) {
  for (var i = 1; i < data.length; i++) {
    var obserJson = {};
    var vars = data[i];
    var dt = new Date(vars[1]);
    //var tmpc = parseFloat(vars[4]);
    var dewpc = parseFloat(vars[4]);

    // process tmpc
    /**
    if (tmpc != 'M') {
      var tmpcRecord = {};
      tmpcRecord.result = tmpc.toString();
      tmpcRecord.phenomenonTime = dt;
      tmpcRecord.Datastream={};
      tmpcRecord.Datastream.id=id_tem;
      tmpcRecords.push(tmpcRecord);
    }
   **/   

    if (dewpc != 'M') {
      var dewRecord = {};
      dewRecord.result = dewpc.toString();
      dewRecord.phenomenonTime = dt;
      dewRecord.Datastream={};
      dewRecord.Datastream.id=id_dew;
      dewRecords.push(dewRecord);
    }
  
  }
  //write observation file to local disk 
  var outputFilename1 = 'iceland/AKUREYRI/tem_observations.json';
  var outputFilename='iceland/AKUREYRI/dew_observations.json';

   fs.writeFile(outputFilename, JSON.stringify(dewRecords), function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("JSON saved to " + outputFilename);
    }
}); 
});
fs.createReadStream(csvFile).pipe(parser);
