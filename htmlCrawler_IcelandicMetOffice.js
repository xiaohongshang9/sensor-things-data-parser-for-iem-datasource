var asynRequest = require('request');
var fs = require('fs');
var select = require('soupselect').select;
var request = require('sync-request');
var format = require('string-format');
format.extend(String.prototype);
var cheerio = require('cheerio');
var util = require('util')
//var stringSearch_backup = process.argv[2];
//iceland weather station names
var iceland_sta_names = ["Akureyri",'Bíldudalur','Egilsstaðaflugvöllur','Grímsey','Hjarðarland','Keflavíkurflugvöllur','Raufarhöfn','Reykjavík','Sauðárkrókur flugvöllur','Skjaldþingsstaðir','Stykkishólmur','Vestmannaeyjabær'];


//use regular expression to match station name with stations in http://en.vedur.is/weather/stations/ and retrive metadata
for (var i = iceland_sta_names.length-1 ; i >= 0; i--) {
  var Station = iceland_sta_names[i];
  var tdFirst = '<td class="name">{}</td>';
  var findDetailRegex = new RegExp('<td title="{}"><a id=.*?class=.*?href="(.*?)">'.format(Station + ' - Information'));
  var attributeRegex = '<tr class="alt"><td>{}<\\/td><td>(.*?)<\\/td><\\/tr>';
  var url = 'http://en.vedur.is/weather/stations/';
  var res = request('GET', url);
  var detailUrl = 'http://en.vedur.is/' + findDetailRegex.exec(res.getBody())[1];
  var table_data=[];
  var res2=request('GET', detailUrl);
  var body2=res2.getBody();
  var attrs = ['Name', 'Type', 'Station number', 'WMO number', "Abbreviation", "Forecast region",
        "Location", "Height above sea-level", "Beginning of weather observations", "Station owned by"];
  $ = cheerio.load(body2);
  var tableBody=$('.infotable').find("tbody>tr").each(function(){
      var row_data = [];
      $('td', this).each(function(){
        row_data.push($(this).text());
      });
       table_data.push(row_data);
  });
  //convert meta data into sensorThings "Thing" format
  var result = {};
  var sensor_Thing={};
  table_data.forEach(function (item) {
    if (attrs.indexOf(item[0]) != -1) {
       result[item[0]] = item[1];
    }
  });
  
  var regExp = /.*\((.*)\)/;
  var sta_coor = regExp.exec(result['Location']);
  console.log(sta_coor[1]);
  var res = sta_coor[1].split(", ");
  var lat=parseFloat(res[0]);
  var lon=parseFloat(res[1]);
  delete result['Location'];
  sensor_Thing["description"]=Station+" Weather Station, Icelandic Met Office";
  sensor_Thing["properties"]=result;
  sensor_Thing["Locations"]=[{}];
  sensor_Thing["Locations"][0]['encodingType']="application/vnd.geo+json";
  sensor_Thing["Locations"][0]['description']=Station+" Weather Station, Icelandic Met Office";

  locationString = '{\"coordinates\":%s\,\"type\":\"%s\"}';
  console.log(util.format(locationString, JSON.stringify([lon,lat]), 'Point'));
  sensor_Thing["Locations"][0]['location'] = util.format(locationString, JSON.stringify([lon,lat]), 'Point');
  
  //post Things to sensorThings service
  var postUrl = 'http://70.75.173.3/OGCSensorThings/v1.0/Things'
  var headers = {'Content-Type' : 'application/json'};
  asynRequest.post({
    uri: postUrl,
    headers: headers,
    body: JSON.stringify(sensor_Thing)
  }, function (e, r, body) {
     var response = JSON.parse(body);
     console.log("ID:"+response.id)
     console.log(JSON.stringify(response, null, '  '));
  });
  //write Things json file to local disk
  console.log(Station);
  var outputFilename = "iceland/Things/"+Station+'.json';
  fs.writeFile(outputFilename, JSON.stringify(sensor_Thing), function (err) {
    if (err) return console.log(err);
  });
}



 