## 1. Synopsis
### htmlCrawler_IcelandicMetOffice.js 
   Used for retriving Iceland weather stations metadata from (http://en.vedur.is/weather/stations/) website. The retrived metadata is processed into "Things" format and posted to sensorThings service (http://70.75.173.3/OGCSensorThings/v1.0/Things)

### IEMCSV_dataParser.js 
   Used for processing CSV data downloaded from (http://mesonet.agron.iastate.edu/request/download.phtml?network=IS__ASOS). CSV data was parsed into "observations" format in line with sensorThings service.

###postObservations.js 
   Used for posting 'observations' json file to sensorThings service (http://70.75.173.3/OGCSensorThings/v1.0/Observations).


## 2. Usage

### htmlCrawler_IcelandicMetOffice.js

    node htmlCrawler_IcelandicMetOffice.js

### IEMCSV_dataParser.js

    node IEMCSV_dataParser.js filepath1

### postObservations.js

    node postObservations.js filepath1

## 3. Dependency

###  htmlCrawler_IcelandicMetOffice.js
     Required node modules:request soupselect sync-request string-format cheerio 

###  IEMCSV_dataParser.js 
     Required node modules: csv path

###  postObservations.js
     Required node modules: request

## 4. Contributors

Xiaohong Shang


